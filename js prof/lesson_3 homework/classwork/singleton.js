/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/
const government = {
  laws:[],
  budget:1000000,
  citizensSatisfactions:0
}

const description = {
  addLaw: (id,name,description) =>{
    government.description = -10;
    government.laws.push({id:id,name:name,description:description});
  },

  readСonstitution: () =>government.laws.forEach(low=>{
    console.log(low);
  }),

  findLow: id =>console.log(government.laws.find(d => d.id === id)),

  showcCitizensSatisfactions: () => console.log(government.citizensSatisfactions),
  
  showBudget: () => console.log(government.budget),

  celebration: () =>{
    government.budget -=50000;
    government.citizensSatisfactions +=5
  }
}
Object.freeze(description);

export default description;