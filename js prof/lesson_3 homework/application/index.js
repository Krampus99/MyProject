/*

  Модули в JS
  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import

  Так как для экспорта и импорта нету родной поддержки в браузерах, то
  нам понадобится сборщик или транспалер который это умеет делать.
  -> babel, webpack, rollup

  На сегодняшний день - самое полулярное решение, это вебпак!

  npm i webpack webpack-cli

  Установка и config-less настройка

  "scripts": {
    "cli": "webpack ./application/index.js --output-path ./public/js --output-filename bundle.js --mode development --color --watch"
  }

*/
 // import imports from "../../classworks/objectfreeze.js"
 // import description from "../../classworks/singleton.js"
 
  `webpack
      ./application/index.js
      --output-path ./public/js
      --output-filename bundle.js
      --mode development
      --color
      --watch
  `;

/*

  npm run cli
  Затестим - в консоли наберем команду webpack

*/

  // import imports from './imports';


 // import description from '../classwork/singleton'

//  description.addLaw(1, 'new law', 'textextetxt');
// description.addLaw(2, 'new law2', 'textextetxt');
// description.addLaw(3, 'new law3', 'textextetxt');
// description.readСonstitution();
// description.findLow(1);
// description.showcCitizensSatisfactions();
// description.celebration();
// description.showBudget();
// description.showcCitizensSatisfactions();


import DeepFreeze from '../classwork/objectfreeze'
let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
        bonuses: ['cookies', 'great look']
    }
};

let FarGalaxy = DeepFreeze(universe);
      // FarGalaxy.good.push('javascript'); // true 
      // FarGalaxy.something = 'Wow!'; // false
      // FarGalaxy.evil.humans = [];   // false

