/*
  Написать медиатор для группы студентов.

  Профессор отвечает только на вопросы старосты.

  У Студента есть имя и группа которой он пренадлежит.
  Он может запросить старосту задать вопрос или получить ответ.

  Староста может добавлять студентов в группу и передавать вопрос профессору.
*/

const Mediator = () => {

  class Professor {
    constructor(name){
      this.name = name;
      this.type = 'professor';

      console.log(`Professor ${this.name} arrived!`);
    }
    answerTheQuestion( question, monitor, student ){

      if( monitor.type !== 'monitor'){
        console.error(`It\' not your bussines, ${student.name}`);
      } else {
        student.getAnswer( `Your question is too philosophical to be answered`, this);
      }
    }
  }

  class Student {
    constructor(name){
      this.name = name;
      this.monitor = null;
      this.type = 'student';

      console.log(`Let's welcome a new student ${this.name}`);
    }
    getAnswer(message, from){      
      console.log(`${from.name} tells ${this.name}: ${message}`);
    }
    tipTheMonitor( message, to, prof){ 
      if (this.monitor !== null){
              
        if (prof !== undefined){
          console.log(`${this.name} tells ${prof.name}: ${message}`);
        } else if ( to !== undefined ){
          console.log(`${this.name} tells ${to.name}: ${message}`);
        }

        this.monitor.askProfessor( message, this, to, prof );

      } else {
        console.warn(`Please ask the monitor to add you to the group!`); 
      }
    }
  }

  // Monitor == Староста
  class Monitor extends Student{
    constructor(name){
      super(name);
      this.name = name;
      this.studentsInGroup = {};
      this.type = 'monitor';

      console.log(`${this.name} will be the monitor of the class`);
    }
    addToGroup(student){ 
      this.studentsInGroup[student.name] = student;
      student.monitor = this;
      console.log(`Student ${student.name} has been added to the group`);      
      console.log(`List of students in the group:`, this.studentsInGroup);      
    }
    askProfessor(message, from, to, prof){

      if ( to !== undefined && to !== this ){ 

        to.answerTheQuestion( message, to, from );
      } else if( to === this ){               
        prof.answerTheQuestion( message, to, from );
      }else {                                 
        for( let key in this.studentsInGroup ){
          if( this.studentsInGroup[key] !== from ){
            this.studentsInGroup[key].getAnswer(message, from);
          }
        }
      }
    }
  }

  const student1 = new Student('Petya');
  const student2 = new Student('Vasya');
  const student3 = new Student('Masha');
  console.log(`***********************`);  
  const prof1 = new Professor('Mr. Albus Dumbledore');
  const prof2 = new Professor('Ms.  Minerva McGonagall');
  console.log(`***********************`);
  const monitor = new Monitor('Gendalf');
  console.log(`***********************`);  
        monitor.addToGroup(student1);
        monitor.addToGroup(student2);
        monitor.addToGroup(student3);
        student3.tipTheMonitor('hello!');
        student1.getAnswer('hello!', prof2);
        student1.tipTheMonitor('Wow! I want more lessons!');
        student2.tipTheMonitor('What are you doing today?', prof2);        
        student1.tipTheMonitor('Can I have additional lessons?', monitor, prof1);

}

export default Mediator;
