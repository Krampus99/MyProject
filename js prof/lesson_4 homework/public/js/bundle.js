/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/composition_vs_inheritance.js":
/*!***************************************************!*\
  !*** ./application/composition_vs_inheritance.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n\r\n  Composition vs Inheritance\r\n  Компоновка (Композиция) против Наследования.\r\n\r\n  Наследование, определяет обьект по тому чем он ЯВЛЯЕТСЯ.\r\n  Композиция, определяет обьект по тому, что он ДЕЛАЕТ\r\n\r\n  Опишем проблематику. Слайды!\r\n\r\n*/\r\n// Our Functions\r\n\r\nconst Composition = () => {\r\n\r\nconst Drive = ( state ) => ({\r\n  drive: () => console.log('Wroooom!, It\\'s a car ' + state.name )\r\n});\r\n\r\nconst ChangeName = state => ({\r\n  changeName: ( name ) => {\r\n    console.log(`Old name:`, state.name, state );\r\n    state.name = name;\r\n    console.log(`New name:`, state.name, state );\r\n  }\r\n});\r\n\r\nconst Refill = ( state ) => ({\r\n  refill: () => console.log( state.name + ' was refiled')\r\n});\r\n\r\nconst Move = ( state ) => ({\r\n  move: ( speed ) => {\r\n    console.log(speed);\r\n    state.speed += speed ;\r\n    console.log( state.name + ' is moving. Speed ->' + state.speed );\r\n  }\r\n});\r\n\r\nconst Fly = ( state ) => ({\r\n  fly: () => {\r\n    console.log( state );\r\n    // state.name = \"qOp\";\r\n    console.log( state.name + ' flying into sky! Weather is ' + state.weather );\r\n  }\r\n});\r\n\r\nconst LoggerIn = obj => ({\r\n  logger: () => {\r\n    console.log( obj );\r\n  }\r\n});\r\n\r\n// Проверим ф-ю\r\nRefill({name: \"Volkswagen\"}).refill(); // Volkswagen was refiled\r\n//\r\n// // Наш конструктор.\r\nconst EcoRefillDrone = (name, speed) => {\r\n  let state = {\r\n    name,\r\n    speed: Number(speed),\r\n    weather: 'rainy'\r\n  };\r\n  return Object.assign(\r\n    {},\r\n    state,\r\n    Drive(state),\r\n    Refill(state),\r\n    ChangeName(state),\r\n    Fly(state),\r\n    Move(state),\r\n    LoggerIn(state)\r\n  );\r\n};\r\n\r\n  const myDrone = EcoRefillDrone('JS-Magic', 100);\r\n        myDrone.drive();\r\n        myDrone.refill();\r\n        myDrone.fly();\r\n        myDrone.move(100);\r\n        myDrone.move(100);\r\n\r\n        // console.log( 'myDrone', myDrone );\r\n        // myDrone.logger();\r\n  //\r\n  // const myDrone2 = EcoRefillDrone('JS-Is-Amaizing', 100);\r\n  // myDrone2.move(100);\r\n  // myDrone2.move(200);\r\n  // myDrone2.changeName('Dex3');\r\n        // myDrone2.changeName('HDMI');\r\n  //\r\n  // const Logger = obj => console.log( obj );\r\n  //       Logger(myDrone2);\r\n  //       myDrone2.logger();\r\n  //\r\n  //\r\n  //       let bindedMove = myDrone2.move.bind(null, 200);\r\n  //       MoveId.addEventListener('click', bindedMove );\r\n\r\n}\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Composition);\r\n\n\n//# sourceURL=webpack:///./application/composition_vs_inheritance.js?");

/***/ }),

/***/ "./application/fabric.js":
/*!*******************************!*\
  !*** ./application/fabric.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n  /*\r\n\r\n      Сегодня разберем некоторые паттерны проектирования JS приложений.\r\n\r\n      Разберем паттерн \"Фабрика\" и то как он работает в JS.\r\n\r\n  */\r\n  const Fabric = () => {\r\n\r\n    class Weapon{\r\n\r\n      render(){\r\n        const root = document.getElementById('root');\r\n        const classMagic = this.magic ? 'magic' : 'regular';\r\n\r\n\r\n        root.innerHTML += `\r\n          <div class=\"${classMagic}\">\r\n            <h2>${this.name}</h2>\r\n            <span>${this.material}</span>\r\n            <div class=\"icon\">\r\n              <img src=\"${this.icon}\" width=\"100\" height=\"100\"/>\r\n            </div>\r\n          </div>\r\n        `;\r\n      }\r\n    }\r\n\r\n    class Sword extends Weapon{\r\n      constructor({name, material, style, magic}){\r\n        super();\r\n        this.weaponType = 'sword';\r\n        this.name = name || 'Unnamed sword';\r\n        this.material = material || 'Steel';\r\n        this.magic = magic !== undefined ? magic : false;\r\n        this.icon = 'images/swords.svg';\r\n      }\r\n    }\r\n\r\n    class Bow extends Weapon{\r\n      constructor({name, material, style, magic}){\r\n        super();\r\n        this.weaponType = 'bow';\r\n        this.name = name || 'Unnamed bow';\r\n        this.material = material || 'Wood';\r\n        this.magic = magic !== undefined ? magic : false;\r\n        this.icon = 'images/archery.svg';\r\n      }\r\n    }\r\n\r\n    class WeaponFactory {\r\n      makeWeapon( weapon ){\r\n        let WeaponClass = null;\r\n        if( weapon.weaponType === 'sword'){\r\n          WeaponClass = Sword;\r\n        } else if( weapon.weaponType === 'bow'){\r\n          WeaponClass = Bow;\r\n        } else {\r\n          return false;\r\n        }\r\n        return new WeaponClass( weapon );\r\n      }\r\n    }\r\n\r\n    const mySuperForge = new WeaponFactory();\r\n\r\n    const MakeMeBlade = mySuperForge.makeWeapon({\r\n      weaponType: 'sword',\r\n      name: 'winner',\r\n      metal: 'dark iron',\r\n      magic: true\r\n    })\r\n    const MakeMeBlade2 = mySuperForge.makeWeapon({\r\n      weaponType: 'sword',\r\n      name: 'defender',\r\n      metal: 'dark iron',\r\n      magic: false\r\n    })\r\n    const MakeMeBow = mySuperForge.makeWeapon({\r\n      weaponType: 'bow',\r\n      name: 'defender',\r\n      metal: 'dark iron',\r\n      magic: false\r\n    })\r\n\r\n    console.log( MakeMeBlade, MakeMeBlade2, MakeMeBow);\r\n    MakeMeBlade.render();\r\n    MakeMeBlade2.render();\r\n    MakeMeBow.render();\r\n    \r\n  }\r\n\r\n\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (Fabric);\r\n\n\n//# sourceURL=webpack:///./application/fabric.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _composition_vs_inheritance__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./composition_vs_inheritance */ \"./application/composition_vs_inheritance.js\");\n/* harmony import */ var _fabric__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fabric */ \"./application/fabric.js\");\n/* harmony import */ var _classwork_task1__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classwork/task1 */ \"./classwork/task1.js\");\n/* harmony import */ var _classwork_task2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../classwork/task2 */ \"./classwork/task2.js\");\n\r\n\r\n\r\n\r\n\r\n// Composition();\r\n// Fabric();\r\n// developer();\r\nObject(_classwork_task2__WEBPACK_IMPORTED_MODULE_3__[\"default\"])()\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classwork/task1.js":
/*!****************************!*\
  !*** ./classwork/task1.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n  Composition:\r\n\r\n  Задание при помощи композиции создать объекты 4х типов:\r\n\r\n  functions:\r\n    - MakeBackendMagic\r\n    - MakeFrontendMagic\r\n    - MakeItLooksBeautiful\r\n    - DistributeTasks\r\n    - DrinkSomeTea\r\n    - WatchYoutube\r\n    - Procrastinate\r\n\r\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\r\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\r\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\r\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\r\n\r\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}\r\n\r\n*/\r\n\r\nconst developer = () =>{\r\n\r\nconst MakeBackendMagic = (state) =>({\r\n MakeBackendMagic:() => console.log(\"MakeBackendMagic\")\r\n});\r\nconst MakeFrontendMagic = (state) =>({\r\n  MakeFrontendMagic:()=> console.log(\"MakeFrontendMagic\")\r\n});\r\nconst MakeItLooksBeautiful = (state) =>({\r\n  MakeItLooksBeautiful:() => console.log(\"MakeItLooksBeautiful\")\r\n});\r\nconst DistributeTasks = (state) =>({\r\n  DistributeTasks :() => console.log(\"DistributeTasks\")\r\n});\r\nconst DrinkSomeTea = (state) =>({\r\n DrinkSomeTea:() => console.log(\"DrinkSomeTea\")\r\n});\r\nconst WatchYoutube = (state) =>({\r\n WatchYoutube:() => console.log(\"WatchYoutube\")\r\n});\r\nconst Procrastinate = (state) =>({\r\n  Procrastinate:() => console.log(\"Procrastinate\")\r\n});\r\n\r\nconst BackendDeveloper = (name,gender,age) =>{\r\n  const state = {\r\n    name,\r\n    gender,\r\n    age ,\r\n    type: 'backend'\r\n  }\r\n\r\n  return Object.assign(\r\n    {},\r\n    state,\r\n    MakeBackendMagic(),\r\n    DrinkSomeTea(),\r\n    Procrastinate()\r\n  );\r\n}\r\n\r\nconst FrontendDeveloper = (name,gender,age) =>{\r\n  const state = {\r\n    name,\r\n    gender,\r\n    age ,\r\n    type: 'frontend'\r\n  }\r\n  return Object.assign(\r\n    {},\r\n    state,\r\n    MakeFrontendMagic(),\r\n    DrinkSomeTea(),\r\n    WatchYoutube()\r\n  );\r\n}\r\n\r\nconst Designer = (name,gender,age) =>{\r\n  const state = {\r\n    name,\r\n    gender,\r\n    age ,\r\n    type: 'designer'\r\n  }\r\n  return Object.assign(\r\n    {},\r\n    state,\r\n    MakeItLooksBeautiful(),\r\n    WatchYoutube(),\r\n    Procrastinate()\r\n  );\r\n}\r\n  const ProjectManager = (name,gender,age) =>{\r\n  const state = {\r\n    name,\r\n    gender,\r\n    age ,\r\n    type: 'manager'\r\n  }\r\n  return Object.assign(\r\n    {},\r\n    state,\r\n    DistributeTasks(),\r\n    Procrastinate(),\r\n    DrinkSomeTea()\r\n  );\r\n}\r\n\r\nlet IvanBack = BackendDeveloper('Ivan','man',22);\r\n\r\nconsole.log(IvanBack)\r\nIvanBack.DrinkSomeTea();\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (developer);\n\n//# sourceURL=webpack:///./classwork/task1.js?");

/***/ }),

/***/ "./classwork/task2.js":
/*!****************************!*\
  !*** ./classwork/task2.js ***!
  \****************************/
/*! exports provided: taskTwo, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"taskTwo\", function() { return taskTwo; });\n/* harmony import */ var _classwork_task1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classwork/task1 */ \"./classwork/task1.js\");\n/*\r\n\r\n  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет\r\n  расспределять и создавать сотрудников компании нужного типа.\r\n\r\n  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\r\n\r\n  HeadHunt => {\r\n    hire( obj ){\r\n      ...\r\n    }\r\n  }\r\n\r\n  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики\r\n\r\n*/\r\n\r\n\r\nconst taskTwo = () => {\r\n\r\n\r\n    const fabric = list => {\r\n      const container = document.getElementById('root');\r\n      let table = `\r\n      <div style = 'width:50%; display:inline-block; text-align:center;'>\r\n      <h3>Доступные сотрудники</h3>\r\n      <table></table>\r\n      </div>\r\n      <div id = 'result' style = 'width:49%; display:inline-block;text-align:center'>\r\n      <h3>Команда</h3>\r\n      </div>\r\n      `\r\n      container.innerHTML = table;\r\n      let hiredArr = [];\r\n\r\n\r\n   function Render(){\r\n    let table = document.querySelector('table');   \r\n    let result = document.getElementById('result') \r\n    list.map (person => {\r\n    let tr = document.createElement('tr');\r\n    let info = `\r\n        <td>\r\n            ${person.name} (${person.age})\r\n        </td>                \r\n        <td>\r\n            ${person.type}\r\n        </td>        \r\n        <td>\r\n            <button data-id=\"${person.name}\">Hire</button>\r\n        </td>\r\n        `\r\n      tr.innerHTML = info \r\n      table.appendChild(tr);\r\n    const btn = table.querySelector(`button[data-id=\"${person.name}\"]`);\r\n          btn.addEventListener('click', hire);\r\n\r\n      function hire(){\r\n        let idx = list.indexOf(person);\r\n        const result = document.getElementById('result');\r\n        const table = document.createElement('table');\r\n        list.splice(idx,1);                \r\n        hiredArr.push(person);\r\n        hiredArr.map(person => {\r\n              let tab =  `\r\n        <tr>\r\n          <td>${person.name}</td>\r\n          <td>${person.type}</td>\r\n          <td>${person.rate} $/hour</td>\r\n        </tr>\r\n        `\r\n          table.innerHTML = tab \r\n     \r\n        });\r\n        result.appendChild(table);\r\n      }  \r\n    });\r\n   }\r\n\r\n    Render();\r\n  }\r\n   \r\n\r\n  let response = fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')\r\n    .then( res => res.json() )\r\n    .then( fabric );\r\n\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (taskTwo);\r\n\n\n//# sourceURL=webpack:///./classwork/task2.js?");

/***/ })

/******/ });