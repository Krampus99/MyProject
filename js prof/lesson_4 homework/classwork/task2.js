/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/
import {BackendDeveloper, FrontendDeveloper, Designer, ProjectManager} from '../classwork/task1'

export const taskTwo = () => {


    const fabric = list => {
      const container = document.getElementById('root');
      let table = `
      <div style = 'width:50%; display:inline-block; text-align:center;'>
      <h3>Доступные сотрудники</h3>
      <table></table>
      </div>
      <div id = 'result' style = 'width:49%; display:inline-block;text-align:center'>
      <h3>Команда</h3>
      </div>
      `
      container.innerHTML = table;
      let hiredArr = [];


   function Render(){
    let table = document.querySelector('table');   
    let result = document.getElementById('result') 
    list.map (person => {
    let tr = document.createElement('tr');
    let info = `
        <td>
            ${person.name} (${person.age})
        </td>                
        <td>
            ${person.type}
        </td>        
        <td>
            <button data-id="${person.name}">Hire</button>
        </td>
        `
      tr.innerHTML = info 
      table.appendChild(tr);
    const btn = table.querySelector(`button[data-id="${person.name}"]`);
          btn.addEventListener('click', hire);

      function hire(){
        let idx = list.indexOf(person);
        const result = document.getElementById('result');
        const table = document.createElement('table');
        list.splice(idx,1);                
        hiredArr.push(person);
        hiredArr.map(person => {
              let tab =  `
        <tr>
          <td>${person.name}</td>
          <td>${person.type}</td>
          <td>${person.rate} $/hour</td>
        </tr>
        `
          table.innerHTML = tab 
     
        });
        result.appendChild(table);
      }  
    });
   }
   

    Render();
  }
   

  let response = fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')
    .then( res => res.json() )
    .then( fabric );

};

export default taskTwo;
