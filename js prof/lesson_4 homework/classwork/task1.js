/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}

*/

const developer = () =>{

const MakeBackendMagic = (state) =>({
 MakeBackendMagic:() => console.log("MakeBackendMagic")
});
const MakeFrontendMagic = (state) =>({
  MakeFrontendMagic:()=> console.log("MakeFrontendMagic")
});
const MakeItLooksBeautiful = (state) =>({
  MakeItLooksBeautiful:() => console.log("MakeItLooksBeautiful")
});
const DistributeTasks = (state) =>({
  DistributeTasks :() => console.log("DistributeTasks")
});
const DrinkSomeTea = (state) =>({
 DrinkSomeTea:() => console.log("DrinkSomeTea")
});
const WatchYoutube = (state) =>({
 WatchYoutube:() => console.log("WatchYoutube")
});
const Procrastinate = (state) =>({
  Procrastinate:() => console.log("Procrastinate")
});

const BackendDeveloper = (name,gender,age) =>{
  const state = {
    name,
    gender,
    age ,
    type: 'backend'
  }

  return Object.assign(
    {},
    state,
    MakeBackendMagic(),
    DrinkSomeTea(),
    Procrastinate()
  );
}

const FrontendDeveloper = (name,gender,age) =>{
  const state = {
    name,
    gender,
    age ,
    type: 'frontend'
  }
  return Object.assign(
    {},
    state,
    MakeFrontendMagic(),
    DrinkSomeTea(),
    WatchYoutube()
  );
}

const Designer = (name,gender,age) =>{
  const state = {
    name,
    gender,
    age ,
    type: 'designer'
  }
  return Object.assign(
    {},
    state,
    MakeItLooksBeautiful(),
    WatchYoutube(),
    Procrastinate()
  );
}
  const ProjectManager = (name,gender,age) =>{
  const state = {
    name,
    gender,
    age ,
    type: 'manager'
  }
  return Object.assign(
    {},
    state,
    DistributeTasks(),
    Procrastinate(),
    DrinkSomeTea()
  );
}

let IvanBack = BackendDeveloper('Ivan','man',22);

console.log(IvanBack)
IvanBack.DrinkSomeTea();
}

export default developer;