/*
  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
            соответсвии с правилавми ниже:

  1. Написать кастомные события которые будут менять статус светофора:
  - start: включает зеленый свет
  - stop: включает красный свет
  - night: включает желтый свет, который моргает с интервалом в 1с.
  И зарегистрировать каждое через addEventListener на каждом из светофоров.

  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
      чтобы включить режим "нерегулируемого перекрестка" (моргающий желтый).

  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)
      или зеленый (на второй клик) цвет соотвественно.
      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

  4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
      статуса, на мигающий желтые.
      Двойной, тройной и более клики на кнопку не должны вызывать повторную
      инициализацию инвервала.

*/
const CustomEvents = () => {
        let trafficLight = document.getElementsByClassName('trafficLight');
        let Arrays = Array.from(trafficLight);
        let RedEvent = new CustomEvent('red');
        let GreenEvent = new CustomEvent('green');
        let YEvent = new CustomEvent('night');

        let intervalFunction = function (elem) {
                return setInterval(function () {
                        if (elem.className === 'trafficLight') {
                                elem.className = 'trafficLight yellow';
                        } else {
                                elem.className = 'trafficLight';
                        }
                }, 300);
        };

        Arrays.forEach(function (item) {
                item.addEventListener('red', function () {
                        clearInterval(item.timer);
                        this.className = 'trafficLight red'
                });

                item.addEventListener('green', function () {
                        clearInterval(item.timer);
                        this.className = 'trafficLight green'
                });

                item.addEventListener('night', function () {
                        this.className = 'trafficLight yellow';
                        clearInterval(item.timer);
                        let self = this;
                        item.timer = intervalFunction(self);
                });
                item.addEventListener('click', function () {
                        if (this.className === 'trafficLight yellow' || this.className === 'trafficLight green' || this.className === 'trafficLight') {
                                this.dispatchEvent(RedEvent);
                        } else if (this.className === 'trafficLight red') {
                                this.dispatchEvent(GreenEvent);
                        }
                });
        });

        window.onload = function () {
                Arrays.forEach(function (item) {
                        item.dispatchEvent(YEvent);
                })
        };

        let button = document.getElementById('Do');

        button.addEventListener('click', function () {
                Arrays.forEach(function (item) {
                        item.dispatchEvent(YEvent);
                })
        });
};

export default CustomEvents;
