// Точка входа в наше приложение
// import Observer from './observer';
// import HOC from './hoc';
// import CustomEvents from './observer/CustomEvents';
import MusicBox from '../classwork/observer';
// import Events from '../classwork/customEvent';


// 0. HOC
// HOC();
// 1. Observer ->
// console.log( Observer );
// Observer();
// console.log('INDEX');
// 2. CustomEvents ->
// CustomEvents();
// Events();
MusicBox();