/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным

    <div class="post">
      <div class="post__title">Some post</div>
      <div class="post__image">
        <img src="https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like!</button>
      </div>
    </div>




*/
const posts = [];

const postsDiv = document.createElement("div");
document.body.appendChild(postsDiv);

class Post {
    constructor(title, image, description, likes) {
        this.id = Math.floor(Math.random()*1000) + "";
        this.title = title;
        this.image = image;
        this.description = description;
        this.likes = likes;
    }

    render() {
        posts.push({
            title: this.title,
            image: this.image,
            description: this.description,
            likes: this.likes,
        })
        const newPost = document.createElement("div");
        newPost.innerHTML =
            `<p><h1>${this.title}</h1>
                <br><img src="${this.image}" width="100px"/>
                <br>${this.description}
                <br><span id="like${this.id}"/>${'\uD83D\uDC4D'} ${this.likes}
            </p>`;
        postsDiv.appendChild(newPost);
        console.log("Standatd Post");
        document.getElementById("like" + this.id)
            .addEventListener("click", ()=>{this.likePost()});
    }

    likePost() {
        this.likes++;
        console.log(this.title, "likes:", this.likes);
        document.getElementById("like" + (this.id))
            .innerText = '\uD83D\uDC4D' + " " + this.likes;
    }
}

class Advertisment extends Post {
    constructor(title, image, description, likes) {
        super(title, image, description, likes);
    }

    render() {
        posts.push({
            title: this.title,
            image: this.image,
            description: this.description,
            likes: this.likes,
        })
        const newPost = document.createElement("div");
        newPost.innerHTML =
            `<p><h2>Advertisement</h2>
                <h1>${this.title}</h1>
                <br><img src="${this.image}" width="200px"/>
                <br>${this.description}
                <br><span id="like${this.id}"> ${'\uD83D\uDC4D'} ${this.likes}</span>
                <br><button id="${posts.length - 1}">Купить</button>
            </p>`;
        postsDiv.appendChild(newPost);
        console.log("Advertisement template");
            document.getElementById(posts.length - 1 + "")
                .addEventListener("click", ()=>{
                    this.buyItem();
                })
        document.getElementById("like" + this.id)
            .addEventListener("click", ()=>{this.likePost()});
    }

    buyItem() {
        console.log(JSON.stringify(this) + " был куплен");
    }
}

const adv1 = new Advertisment("Покупайте наших слонов!", "http://www.wallon.ru/_ph/10/700503798.jpg", "самые низкие цены!", 0);
adv1.render();
adv1.buyItem();

const post1 = new Post("Apple", "https://itc.ua/wp-content/uploads/2017/09/Znimok-ekrana-2017-09-12-o-13.59.07.png", "вкусное яблоко", 0);

post1.render();
console.log(post1.likes);
post1.likePost();
console.log(post1.likes);
console.log(post1);

const adv2 = new Advertisment("ttt", "sds", "dsf", 0);
adv2.likePost();
adv2.render();
