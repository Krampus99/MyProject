/*

  Задание - используя классы и (или) прототипы создать программу, которая будет
  распределять животных по зоопарку.

  Zoo ={
    name: '',
    AnimalCount: 152,
    zones: {
      mammals: [],
      birds: [],
      fishes: [],
      reptile: [],
      others: []
    },
    addAnimal: function(animalObj){
      // Добавляет животное в зоопарк в нужную зону.
      // зона берется из класса который наследует Animal
      // если у животного нету свойства zone - то добавлять в others
    },
    removeAnimal: function('animalName'){
      // удаляет животное из зоопарка
      // поиск по имени
    },
    getAnimal: function(type, value){
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска
    },
    countAnimals: function(){
      // функция считает кол-во всех животных во всех зонах
      // и выводит в консоль результат
    }
  }

  Есть родительский класс Animal у которого есть методы и свойства:
  Animal {
    name: 'Rex', // имя животного для поиска
    phrase: 'woof!',
    foodType: 'herbivore' | 'carnivore', // Травоядное или Плотоядное животное
    eatSomething: function(){ ... }
  }

  Дальше будут классы, которые расширяют класс Animal - это классы:
  - mammals
  - birds
  - fishes
  - pertile

  каждый из них имеет свои свойства и методы:
  в данном примере уникальными будут run/speed. У птиц будут методы fly & speed и т.д
  Mammals = {
    zone: mamal, // дублирует название зоны, ставиться по умолчанию
    type: 'wolf', // что за животное
    run: function(){
      console.log( wolf Rex run with speed 15 km/h );
    },
    speed: 15
  }

  Тестирование:
    new Zoo('name');
    var Rex = new Mammal('Rex', 'woof', 'herbivore', 15 );
    your_zooName.addAnimal(Rex);
      // Добавит в your_zooName.zones.mamals новое животное.
    var Dex = new Mammal('Dex', 'woof', 'herbivore', 11 );
    your_zooName.addAnimal(Dex);
      // Добавит в your_zooName.zones.mamals еще одно новое животное.

    your_zooName.get('name', 'Rex'); -> {name:"Rex", type: 'wolf' ...}
    your_zooName.get('type', 'wolf'); -> [{RexObj},{DexObj}];

    Программу можно расширить и сделать в виде мини игры с интерфейсом и сдать
    как курсовую работу!
    Идеи:
    - Добавить ранжирование на травоядных и хищников
    - добавив какую-то функцию которая иммитирует жизнь в зоопарке. Питание, движение, сон животных и т.д
    - Условия: Если хищник и травоядный попадает в одну зону, то хищник сьедает травоядное и оно удаляется из зоопарка.
    - Графическая оболочка под программу.
*/
class Animal{
  constructor(name, phrase, foodType){
    this.name = name,
    this.phrase = phrase,
    this.foodType = foodType
  }

  eatSomething(){
    console.log('IT EATS!!!')
  }
}

class Mammal extends Animal{
  constructor(name, phrase, foodType, species, speed){
    super(name, phrase, foodType);
    this.zone = 'mammals',
    this.species = species,
    this.speed = speed 
  }
  run(){
    console.log(`${this.species} named ${this.name} runs with speed ${this.speed}`);
  }
}

class Bird extends Animal{
  constructor(name, phrase, foodType, species, height){
    super(name, phrase, foodType);
    this.zone = 'birds',
    this.species = species,
    this.height = height 
  }

  fly(){
    console.log(`${this.species} named ${this.name} flies on height ${this.height}`);
  }
}

class Fish extends Animal{
  constructor(name, phrase, foodType, species, speed){
    super(name, phrase, foodType);
    this.zone = 'fishes',
    this.species = species,
    this.speed = speed 
  }

  foat(){
    console.log(`${this.species} named ${this.name} foats with speed ${this.speed}`);
  }
}

class Reptile extends Animal{
  constructor(name, phrase, foodType, species){
    super(name, phrase, foodType);
    this.zone = 'reptiles',
    this.species = species
  }

  lookUgly(){
    console.log(`${this.species} named ${this.name} started to look ugly (actually they always look ugly)`);
  }
}

class Zoo{
  constructor(name){
    this.AnimalCount = 0;
    this.zones = {
      mammals: [],
      birds: [],
      fishes: [],
      reptiles: [],
      others: []
    }
  }

  addAnimal(animalObj){
    let {zone} = animalObj;
    let addedCheck = false;

    for(let key in this.zones){
      if (key === zone){
        this.zones[key].push(animalObj);
        addedCheck = true;
      }      
    }
    if (addedCheck === false){
      this.zones.others.push(animalObj);
    }
    this.countAnimals()
  }

  removeAnimal(animalName){
      for (let zone in this.zones){
        this.zones[zone].map(item => {
          if (item.name.toLowerCase() === animalName.toLowerCase()){
            let index = this.zones[zone].indexOf(item);
            this.zones[zone].splice( index, 1 );
          }
        });
      }
      this.countAnimals()
  }   

  getAnimal(type, value){
    if (type === 'name'){
      for (let zone in this.zones){
        this.zones[zone].map(item => {
          if (item.name.toLowerCase() === value.toLowerCase()){
            console.log(item);
          }
        });
      }
    }
    else if (type === 'species'){
      for (let zone in this.zones){
        this.zones[zone].map(item => {
          if (item.species.toLowerCase() === value.toLowerCase()){
            console.log(item);
          }
        });
      }
    }
  }

  countAnimals(){    
      this.AnimalCount = 0;    
      for (let zone in this.zones){
        this.AnimalCount += this.zones[zone].length
      }
  }
}
let zoo = new Zoo('Kyiv mega zoo');

let Rex = new Mammal('Rex', `Woooooo`, 'carnivore', `wolf`, 35);
zoo.addAnimal(Rex);

let Dex = new Mammal('Dex', 'woof', 'herbivore', 'wolf', 11 );
zoo.addAnimal(Dex);

let eagle = new Bird('Zeus', `Screeeeeeeeeeeeeech`, 'carnivore', `eagle`, 1000);
zoo.addAnimal(eagle);

let reptile = new Reptile('Oscar', `Yooohooo`, 'carnivore', `lizard`);
zoo.addAnimal(reptile);


zoo.getAnimal('name', 'Rex');
zoo.getAnimal('species', 'wolf');
zoo.removeAnimal('Zeus');
console.log(zoo);
eagle.fly()
