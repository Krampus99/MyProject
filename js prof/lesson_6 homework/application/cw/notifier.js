export class Notifier {
	send (msg, node, name){
		console.log(`message has been sent: ${msg} via ${name}`);
		const target = node.querySelector(`.notifier__item[data-name="${name}"]`);
		target.innerHTML += `<div>${msg}</div>`;
	}
}

export class SmsNotifier extends Notifier {
  send( msg, node, name = 'sms' ){
    super.send(msg, node, name);
  }
}

export class ViberNotifier extends Notifier {
  send( msg, node, name = 'viber'){
      super.send(msg, node, name);
  }
}

export class GmailNotifier extends Notifier {
  send( msg, node, name = 'gmail' ){
      super.send(msg, node, name);
  }
}

export class TelegramNotifier extends Notifier {
  send( msg, node, name = 'telegram' ){
      super.send(msg, node, name);
  }
}

export class SlackNotifier extends Notifier {
  send( msg, node, name = 'slack' ){
      super.send(msg, node, name);
  }
}

export class SkypeNotifier extends Notifier {
  send( msg, node, name = 'skype' ){
      super.send(msg, node, name);
  }
}

export class MessengerNotifier extends Notifier {
  send( msg, node, name = 'messenger' ){
      super.send(msg, node, name);
  }
}

export default Notifier;
