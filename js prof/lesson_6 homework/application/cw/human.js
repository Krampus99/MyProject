export  function Human( name ){
          this.name = name;
          this.currentTemperature = 0;
          this.minTemperature = -10;
          this.maxTemperature = 50;

          console.log( this, `new Human ${this.name} arrived!`);
        }

        Human.prototype.ChangeTemperature = function( changeValue ){
          console.log(`current temperature has changed on ${changeValue} degrees`);
          this.currentTemperature = this.currentTemperature + changeValue;


          if ( this.currentTemperature >= 0 && this.currentTemperature <= this.maxTemperature){            
            console.log(`It's getting hot make sure to have enogh coolers (current temp: ${this.currentTemperature}, max. temp: ${this.maxTemperature})`)
          } else if ( this.currentTemperature > this.maxTemperature ) {
            if (this.coolers.length !== 0){                                          
              while ( this.currentTemperature > this.maxTemperature && this.coolers.length !== 0) {
                this.currentTemperature -= Number(this.coolers[0].temperatureCoolRate);
                console.log(`You used one of your coolers. It was ${this.coolers[0].name}. Current temp is: ${this.currentTemperature}, ${this.coolers.length-1} coolers remaining`);
                this.coolers.splice(0,1);
              };
              
               if (this.currentTemperature > this.maxTemperature) {
                if (this.coolers.length === 0) {
                  console.error(`temp is too hot (${this.currentTemperature}) and you didn't have enough coolers so you have burnt alive :(`)
                } 
              }
            } else if (this.coolers.length === 0) {
              console.error(`temp is too hot (${this.currentTemperature}) and you didn't have enough coolers so you have burnt alive :(`)
            }
          } else if( this.currentTemperature < this.minTemperature ){
            console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
          } else if ( this.currentTemperature < 0 ) {
            console.log(`It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);
          }
        };

export default Human;
