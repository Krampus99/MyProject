export class NotifTarget {
	constructor(name){
		this.name = name;
		this.image = `images/${this.name}.svg`
	}
}

export default NotifTarget;
