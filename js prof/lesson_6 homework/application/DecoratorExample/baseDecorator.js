import { SmsNotifier, ViberNotifier, GmailNotifier, TelegramNotifier } from './Notifier';

class BaseDecorator {
  constructor( clients ){
    let obs = clients.map( obs => {
      if( obs.name === 'sms' ){
        return new SmsNotifier(obs);
      } else if( obs.name === 'mail'){
        return new GmailNotifier(obs);
      } else if( obs.name === 'telegram'){
        return new TelegramNotifier(obs);
      } else if( obs.name === 'viber'){
        return new ViberNotifier(obs);
      }
    })
    this.clients = obs;
  }
  sendMessage( msg ){
    this.clients.map( ( obs ) => {
      obs.send(msg);
    });
  }

  addNotifier( notifier ){
    this.clients.push( notifier );
  }
}


export default BaseDecorator;
