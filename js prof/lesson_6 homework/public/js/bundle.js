/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/cw/Application.js":
/*!***************************************!*\
  !*** ./application/cw/Application.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Decorator */ \"./application/cw/Decorator.js\");\n/* harmony import */ var _NotifTarget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./NotifTarget */ \"./application/cw/NotifTarget.js\");\n\r\n\r\n\r\nclass Application {\r\n\r\n\tconstructor(){\r\n\r\n\t\tthis.notifierTargets = [];\t\t\r\n\t\tthis.renderInterface = this.renderInterface.bind(this);\r\n\t\tthis.node = null;\r\n\t\tif (arguments.length !== 0){\t\t\t\r\n\t\t\tlet args = Array.from(arguments);\r\n\t\t\t\targs.map(arg => {\r\n\t\t\t\t\tlet notifElem = new _NotifTarget__WEBPACK_IMPORTED_MODULE_2__[\"default\"](arg);\r\n\t\t\t\t\tthis.notifierTargets.push(notifElem);\r\n\t\t\t\t});\r\n\t\t}\r\n\t\tthis.notifier = new _Decorator__WEBPACK_IMPORTED_MODULE_0__[\"default\"](this.notifierTargets);\t\t\r\n\t}\r\n\r\n\trenderInterface(){\r\n\t\tconst root = document.getElementById('root');\r\n\r\n\t\tconst appWrap = document.createElement('section');\r\n\t\tappWrap.classList.add('notifier_app');\r\n\r\n\t\tappWrap.innerHTML =\r\n\t\t`\r\n\t\t<div class=\"notifier_app--container\">\r\n\t\t\t<header>\r\n\t\t\t\t<input type=\"text\" class=\"notifier__messanger\" />\r\n\t\t\t\t<button class=\"notifier__send\">Send Message</button>\r\n\t\t\t</header>\r\n\t        <div class=\"notifier__container\">\r\n\t        ${\r\n\t          this.notifierTargets.map( item =>\r\n\t            `\r\n\t            <div class=\"notifier__item\" data-name=\"${item.name}\">\r\n\t              <header class=\"notifier__header\">\r\n\t                <img width=\"25\" src=\"${item.image}\"/>\r\n\t                <span>${item.name}</span>\r\n\t              </header>\r\n\t              <div class=\"notifier__messages\"></div>\r\n\t            </div>\r\n\t            `).join('')\r\n\t        }\r\n\t        </div>\t\t\t\r\n\t\t</div>\r\n\t\t`;\r\n    \tconst btn = appWrap.querySelector('.notifier__send');\r\n    \tconst input = appWrap.querySelector('.notifier__messanger');\r\n    \t\tthis.node = appWrap;\r\n\t    \tbtn.addEventListener('click', () => {\r\n\t    \t\tlet value = input.value;\r\n\t    \t\tthis.notifier.sendMessage(value, this.node);\r\n\t    \t});\r\n\t\t    \r\n\t    root.appendChild(appWrap);\r\n\r\n\t}\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Application);\r\n\n\n//# sourceURL=webpack:///./application/cw/Application.js?");

/***/ }),

/***/ "./application/cw/Decorator.js":
/*!*************************************!*\
  !*** ./application/cw/Decorator.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _notifier__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./notifier */ \"./application/cw/notifier.js\");\n\r\n\r\nclass BaseDecorator {\r\n  constructor( clients ){\r\n    let obs = clients.map( obs => {\r\n      if( obs.name === 'sms' ){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_1__[\"SmsNotifier\"](obs);\r\n      } else if( obs.name === 'gmail'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_1__[\"GmailNotifier\"](obs);\r\n      } else if( obs.name === 'telegram'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_1__[\"TelegramNotifier\"](obs);\r\n      } else if( obs.name === 'viber'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_1__[\"ViberNotifier\"](obs);\r\n      } else if( obs.name === 'slack'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_1__[\"SlackNotifier\"](obs);\r\n      } else if( obs.name === 'skype' ){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_1__[\"SkypeNotifier\"](obs);\r\n      } else if( obs.name === 'messenger' ){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_1__[\"MessengerNotifier\"](obs);\r\n      }\r\n\r\n    });\r\n    this.clients = obs;\r\n  }\r\n  sendMessage( msg, node ){\r\n    this.clients.map( ( obs ) => {\r\n      obs.send( msg, node );\r\n    });\r\n  }\r\n}\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (BaseDecorator);\r\n\n\n//# sourceURL=webpack:///./application/cw/Decorator.js?");

/***/ }),

/***/ "./application/cw/NotifTarget.js":
/*!***************************************!*\
  !*** ./application/cw/NotifTarget.js ***!
  \***************************************/
/*! exports provided: NotifTarget, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"NotifTarget\", function() { return NotifTarget; });\nclass NotifTarget {\r\n\tconstructor(name){\r\n\t\tthis.name = name;\r\n\t\tthis.image = `images/${this.name}.svg`\r\n\t}\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (NotifTarget);\r\n\n\n//# sourceURL=webpack:///./application/cw/NotifTarget.js?");

/***/ }),

/***/ "./application/cw/notifier.js":
/*!************************************!*\
  !*** ./application/cw/notifier.js ***!
  \************************************/
/*! exports provided: Notifier, SmsNotifier, ViberNotifier, GmailNotifier, TelegramNotifier, SlackNotifier, SkypeNotifier, MessengerNotifier, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Notifier\", function() { return Notifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SmsNotifier\", function() { return SmsNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ViberNotifier\", function() { return ViberNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"GmailNotifier\", function() { return GmailNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TelegramNotifier\", function() { return TelegramNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SlackNotifier\", function() { return SlackNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SkypeNotifier\", function() { return SkypeNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MessengerNotifier\", function() { return MessengerNotifier; });\nclass Notifier {\r\n\tsend (msg, node, name){\r\n\t\tconsole.log(`message has been sent: ${msg} via ${name}`);\r\n\t\tconst target = node.querySelector(`.notifier__item[data-name=\"${name}\"]`);\r\n\t\ttarget.innerHTML += `<div>${msg}</div>`;\r\n\t}\r\n}\r\n\r\nclass SmsNotifier extends Notifier {\r\n  send( msg, node, name = 'sms' ){\r\n    super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass ViberNotifier extends Notifier {\r\n  send( msg, node, name = 'viber'){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass GmailNotifier extends Notifier {\r\n  send( msg, node, name = 'gmail' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass TelegramNotifier extends Notifier {\r\n  send( msg, node, name = 'telegram' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass SlackNotifier extends Notifier {\r\n  send( msg, node, name = 'slack' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass SkypeNotifier extends Notifier {\r\n  send( msg, node, name = 'skype' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass MessengerNotifier extends Notifier {\r\n  send( msg, node, name = 'messenger' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Notifier);\r\n\n\n//# sourceURL=webpack:///./application/cw/notifier.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_task2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classworks/task2 */ \"./classworks/task2.js\");\n// import DecoratorExample from './DecoratorExample';\r\n// import DecoratorBase from './decorator';\r\n// import BeachParty from '../classworks/task1';\r\n\r\n\r\n/*\r\n  Демо декоратора\r\n*/\r\n// DecoratorBase();\r\n// DecoratorExample();\r\n// BeachParty();\r\nObject(_classworks_task2__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/task2.js":
/*!*****************************!*\
  !*** ./classworks/task2.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _application_cw_Application__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/cw/Application */ \"./application/cw/Application.js\");\n/* harmony import */ var _application_cw_notifier__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../application/cw/notifier */ \"./application/cw/notifier.js\");\n\n/*\n  Повторить задание с оповещаниями (application/DecoratorExample), с\n  использованием нескольких уровней абстракций, а именно паттерны:\n  Decorator, Observer, Fabric\n\n\n  Задача: Написать динамичную систему оповещений, которая будет отправлять\n  сообщения все подписаным на неё \"Мессенджерам\".\n  Картинки мессенджеров есть в папке public/images\n\n  Класс оповещения должен иметь декоратор на каждый мессенджер.\n\n  При создании обьекта класса Application нужно передавать обьект\n  в котором будут находится те \"Мессенджеры\" который в результате будут\n  подписаны на этот блок приложения.\n\n  Отправка сообщения по \"мессенджерам\" должна происходить при помощи\n  паттерна Observer.\n\n  При отправке сообщения нужно создавать обьект соответствующего класса,\n  для каждого типа оповещания.\n\n  let header = new Application('slack', 'viber', 'telegramm');\n  let feedback = new Application('skype', 'messanger', 'mail', telegram);\n\n  btn.addEventListener('click', () => header.sendMessage(msg) );\n\n  Архитектура:\n\n  Application( messanges ) ->\n    notfier = new Notifier\n    renderInterface(){...}\n\n  Notifier ->\n    constructor() ->\n      Fabric-> Фабрикой перебираете все типы месенджеров которые\n      подписаны на эту Application;\n    send() -> Отправляет сообщение всем подписчикам\n*/\n\n\n\nlet NotificationApp = () => {\n  let header = new _application_cw_Application__WEBPACK_IMPORTED_MODULE_0__[\"default\"]('slack', 'viber', 'telegram', 'skype', 'messenger'); //Сделал конструктор объекта из аргументов, имхо так удобнее (см. NotifTarget.js)\n  let feedback = new _application_cw_Application__WEBPACK_IMPORTED_MODULE_0__[\"default\"]('skype', 'messenger', 'gmail', 'telegram');\n  header.renderInterface();\n  feedback.renderInterface();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (NotificationApp);\n\n\n//# sourceURL=webpack:///./classworks/task2.js?");

/***/ })

/******/ });