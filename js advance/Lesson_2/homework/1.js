
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
  var buttonContainer = document.getElementById('buttonContainer');
  var button = document.querySelectorAll('button');
  var tabContein = document.getElementById('tabContainer');
    var div = tabContein.querySelectorAll('div');  
  for(let i=0;i<button.length;i++){
    if(button[i].dataset.tab === '1'){
      var button1 = button[i];
       button1.onclick = function(event) {
    div[0].classList.toggle('active');
          }
    }
    else if(button[i].dataset.tab === '2'){
      var button2 = button[i];
      button2.onclick = function(event) {
    div[1].classList.toggle('active');
          }
    }
    else{
      var button3 = button[i];
      button3.onclick = function(event) {
    div[2].classList.toggle('active');
          }
    }

  }
  var buttonHide = document.createElement('button');
  buttonHide.innerText = ('hideAllTabs');
  buttonHide.classList.add ('showButton');
    buttonContainer.appendChild(buttonHide); 
  buttonHide.onclick = function(){
    for(let i=0;i<div.length;i++){
      div[i].classList.remove('active');
    }
  }
          
          