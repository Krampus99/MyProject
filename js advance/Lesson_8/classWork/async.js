/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
var url = "http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2";

async function first() {
	var companyList = await fetch(url);
	companyList = await companyList.json();
	console.log(companyList)
	companyList = companyList.map((item,company,balance,address,registered)=>{
		return{
			company:item.company,
			balance:item.balance,
			address:item.address,
			registered:item.registered
		}
	})
// 	forEach.companyList(function as(i) {
// 	const selectedUserName = companyList[i];
// 	let {company,balance,address,registered
// } = selectedUserName;
// var  CombinedUser={
// 	company,
// 	balance,
// 	address,
// 	registered

// }
// })
console.log(companyList)
		
}
first(url);