/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
function Comment(name,text,avatarUrl) {
  this.name = name;
  this.text = text;
  this.avatarUrl = avatarUrl;
  this.likes = 0;
}
Comment.prototype.defaultAvatarUrl ="https://cdn.dribbble.com/users/332085/screenshots/1620589/wallpaper_dali_bleu.png";
Comment.prototype.likesAdd = function(ev){
  document.getElementById(ev.target.id).innerText = `likes: ${++this.likes}`;
}
var myComment1 = new Comment('CommentName','CommentText');
var myComment2 = new Comment("commentName", "CommentText", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhFEXGsj2SMxYbxQSMv6lo_vNti3Xb5ozWgxNKuliqV3GDdL7P");
var myComment3 = new Comment("commentName", "CommentText", "https://is1-ssl.mzstatic.com/image/thumb/Purple30/v4/5d/53/31/5d5331a2-81a2-4ef9-82f0-83c86cdb6567/mzl.raxewdll.jpg/246x0w.jpg");
var myComment4 = new Comment("commentName", "CommentText", "http://profilepicturesdp.com/wp-content/uploads/2018/07/profile-pictures-avatars-4.png");

console.log(myComment1);
console.log(myComment2);
console.log(myComment3);
console.log(myComment4);

var CommentsArray = [myComment1,myComment2,myComment3,myComment4];


let div = document.createElement('div');
div.id = "CommentsFeed";
document.body.appendChild(div);

function renderComments(comments) {
  comments.forEach(comment=>{
  let newComment = document.createElement('h2');
  newComment.innerText = comment.name;
  div.appendChild(newComment);
  let newCommentText = document.createElement('p');
  newCommentText.innerText = comment.text;
  div.appendChild(newCommentText);
  let br = document.createElement('br');
  div.appendChild(br);
  let newImg = document.createElement('img');
  newImg.width = '400';
  newImg.src = (Boolean(comment.avatarUrl)) ? comment.avatarUrl : comment.defaultAvatarUrl;
  div.appendChild(newImg);
  let br2 = document.createElement('br');
  div.appendChild(br2);
  let newButton = document.createElement('Button');
  newButton.id = Math.floor(Math.random()*10);
  newButton.innerText = `likes : ${comment.likes}`;
  const likesAdd = comment.likesAdd.bind(comment);
  newButton.addEventListener('click', likesAdd);
  div.appendChild(newButton);
})
}
 renderComments(CommentsArray);