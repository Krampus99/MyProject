/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/
function Dog(name,kind) {
  this.name = name;
  this.kind = kind;
  this.state = 'stay';
  this.Go = function(){
    this.state = 'go';
  console.log(this.name + ' ' + this.state);
}
this.Eat = function(){
  this.state = ' Eat';
  console.log(this.name + ' ' + this.state);
}
this.showAllProperties = function(){
  for (key in this){
    console.log(`${key}: ${this[key]}`);
  }
}
}

var alf = new Dog('alfred','ciam');
console.log(alf);
alf.showAllProperties();