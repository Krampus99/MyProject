/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
var Train = {};
Train.train_name = 'Abs';
Train.speed = 40;
Train.passenger = 100;
Train.go = function(){
	console.log('Поезд :' + this.train_name + ' Везет :' + this.passenger +' пассажиров,'+' Со скоростью :'+this.speed );
}
Train.stop = function(){
	this.speed = 0;
	console.log('Поезд '+this.train_name+' остановился.' + 'Скорость :'+this.speed);
}
Train.get_passenger = function( newPass ){
	this.passenger += Number(newPass);
	console.log('Подобрали пассажиров, теперь пассажиров :'+ this.passenger)
}

Train.go();
Train.get_passenger('73');